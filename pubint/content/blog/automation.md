---
title: "automating properties"
author: ["Stefan Marx"]
date: 2022-03-09T20:00:00+01:00
draft: false
images: ["/images/blog/test4.jpg"]
---

Here we show some code:

<!--more-->

```emacs-lisp
(use-package dired
  :ensure nil
  :commands (dired dired-jump)
  :bind (("C-x C-j" . dired-jump))
  :custom ((dired-listing-switches "-agho --group-directories-first")))
```

Just some codeblock example...
