---
title: "If it works now?"
author: ["Stefan Marx"]
date: 2022-03-10T13:18:00+01:00
draft: false
images: ["/images/blog/default.jpg"]
---

I guess this is one of the last building blocksd that I need for this to function.

<!--more-->

A template definition for org-tempo:

```emacs-lisp
(require 'org-tempo)
(tempo-define-template "properties" ; just some name for the template
       '(":PROPERTIES:" n
	 "  :EXPORT_FILE_NAME: " p n
	 "  :EXPORT_HUGO_CUSTOM_FRONT_MATTER: :images '(/images/)'" n
	 ":END:")
       "<T"
       "Insert a properties drawer for ox_hugo" ; documentation
       'org-tempo-tags)
```

And it just worked. Only thing to solve is to get rid of formatting inside the properties drawer, so the path does not show up in _italics_. Google will be my friend ;-)
