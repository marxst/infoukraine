---
title: "Privacy Policy"
author: ["Stefan Marx"]
date: 2022-03-08T17:29:00+01:00
draft: false
---

The privacy policy of this site is very simple:

-   no cookies
-   no embedded content of any kind
-   no forms
-   no logging

...

-   No worries.
