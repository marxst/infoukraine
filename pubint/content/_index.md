---
title: "This is the homepage"
date: 2022-03-05T01:06:21+01:00
---
<style>
.main-image {
    background-position: center;
    background-size: cover;
    background-image: url(/images/indexhero.jpg);
}
</style>
<section class="hero is-large is-dark main-image">
    <div class="hero-body">
        <div class="container has-text-centered">
            <h2 class="title is-1">Are you ready?</h2>
        </div>
    </div>
</section>


{{< section >}}
## Some title

My paragraph.
{{< /section >}}

{{< blogposts count=6 >}}
