---
title: "Contact Information"
author: ["Stefan Marx"]
date: 2022-03-09T11:00:00+01:00
draft: false
layout: "contact"
heroimage: "/images/contact.jpg"
buttons:
- name: Phone
  icon: fas fa-phone
  href: "tel:123456789"
- name: Contact email
  icon: fas fa-envelope
  id: open-modal
---
