---
title: "About Me"
author: ["Stefan Marx"]
date: 2022-03-09T19:47:00+01:00
draft: false
heroimage: "/images/about.jpg"
herotext: "What is fake, what is true, how to find trust."
layout: "about"
---

I am just some normal guy with some IT skills who want to show some others, how to find and verify information in the internet.

What is false, what is true, how can I be sure what I am looking at? These are difficult questions for everyone, and the social media with its filter bubble creation algorithms do not really help either.

It is so easy nowadays to convince yourself of some theory and then find absolute confirmation bias without noticing, that you just locked yourself into an echo chamber.

I want to help solving this problem by providing links to trustable sources and pointers to methods for self verification of sources.
